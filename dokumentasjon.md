# Om min innlevering

I min innlevering har jeg gått for en løsning hvor komponentene er løst koblet sammen og utbyttbare. Ingenting er så kjedelig som å måtte prøve å nøste opp tette relasjoner mellom klasser, så derfor foretrekker jeg heller å programmere mot abstraksjoner, altså interfaces. På denne måten vet jeg hvilket problem en klasse som implementerer interfacet vil forsøke å løse, men ikke nødvendigvis hvordan.

## Relasjoner
For å holde det mest mulig likt virkeligheten er det `CarRentalService` som har ansvaret for å holde orden på kundene, bilene og utleie. Bilene har ingen kunnskap om sin egen status eller om hvem som ev. måtte ha leid dem. Kundene vet ikke om de har leid bil eller ev. hvilken bil, men de har muligheten til å spørre utleier om dette.

## Notifikasjoner
`CarRentalService` er en subklasse av `java.util.Observable` som gir muligheten til å sende notifikasjoner til ev. abonnenter om en handling som har skjedd. Da jeg var usikker på om jeg skulle gå for en konsoll-versjon eller skrive GUI virket dette som en måte å holde det løst koblet på, samt enkelt å ha flere abonnenter.

## Trådhåndtering
Jeg har holdt dette på et enkelt og lavt nivå, det er kun 2 klasser som får tildelt egne tråd(er) utenom main, og det er `Customer` og `ThreadStarter`. Sistnevnte er kun til for å starte de 5 første kundetrådene etter man har tastet inn navn.

Klassen `Customer` gjør 2 handlinger i en evig loop: låne en bil samt levere den tilbake. Innimellom dette sover tråden et spesifisert antall millisekunder.

Da det er klassen `CarRentalService` som håndterer forespørslene fra de 10 kundene er det her fokuset på trådsikkerhet må ligge. Per nå er det 1 lock som befinner seg i denne klassen og den brukes to steder. For å holde det strengt låses den både i metoden `rentItem()` og `returnItem()`, da dette er kritiske områder.

Listen over utleiestatus *(`CarRentalService.rentalStatus`)* er også definert til å være synkronisert. Da det er flere metoder utenom `rentItem()` og `returnItem()` som kan komme til å benytte dette attributtet så jeg det som greit å gi den denne egenskapen.

I klassen `Customer` benyttes det et enkelt flagg som bestemmer om en tråd skal fortsette å kjøre etter endt gjennomgang, eller om den skal stoppe. Dette for både å holde kontroll på tråden via selve objektet samt for testingens skyld. Det er vanskelig å stoppe en kjørende tråd som går i evig loop på et spesifikt sted samtidig som man ønsker å teste på det.

### Signalisering mellom tråder
Dersom det er tomt for biler å leie vil tråden bli bedt om å vente gjennom en Condition. I metoden `returnItem()` signaliseres det etter en retur at det fins biler tilgjengelig.
