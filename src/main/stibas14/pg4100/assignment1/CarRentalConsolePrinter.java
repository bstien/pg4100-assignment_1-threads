package stibas14.pg4100.assignment1;

import java.util.Map;
import java.util.Observable;
import java.util.Observer;

/**
 * A class that observes changes in CarRentalService and prints information
 * accordingly to the console
 */
public class CarRentalConsolePrinter implements Observer {

	@Override
	public void update(Observable o, Object arg)
	{
		// Since there's only 1 Observable in this application I justify
		// the casting without properly checking.
		CarRentalServiceNotification notification = (CarRentalServiceNotification) arg;

		// Fetch all knowns.
		CarRentalService rentalService = (CarRentalService) o;
		Customer customer = notification.getCustomer();
		String carIdentifier = notification.getCarIdentifier();
		Map<String, RentalStatus> rentalStatus = rentalService.rentalStatus();

		// Figure out which method to call.
		if ( "rental".equals(notification.getType()) )
		{
			carRented(customer, carIdentifier, rentalStatus);
		}
		else if ( "return".equals(notification.getType()) )
		{
			carReturned(customer, carIdentifier, rentalStatus);
		}
		else if ( "no_available_cars".equals(notification.getType()) )
		{
			noCarAvailable(customer, rentalStatus);
		}
	}

	public void carReturned(Customer customer, String carIdentifier, Map<String, RentalStatus> rentalStatus)
	{
		System.out.println("** A car has been returned: ");
		String out = customer.getName() + " returned " + carIdentifier;
		System.out.println(out);

		printStatus(rentalStatus);
	}

	public void carRented(Customer customer, String carIdentifier, Map<String, RentalStatus> rentalStatus)
	{
		System.out.println("** A car has been rented: ");
		String out = customer.getName() + " rented " + carIdentifier;
		System.out.println(out);

		printStatus(rentalStatus);
	}

	public void noCarAvailable(Customer customer, Map<String, RentalStatus> rentalStatus)
	{
		System.out.println("** No car available for " + customer.getName());

		printStatus(rentalStatus);
	}

	private void printStatus(Map<String, RentalStatus> status)
	{
		System.out.println("**** Begin status ****");
		int i = 1;
		for (String carIdentifier : status.keySet())
		{
			boolean available = status.get(carIdentifier).isAvailable();
			String out = carIdentifier + " [" + (available ? " " : "X") + "]";
			if ( i % 2 == 0 )
			{
				System.out.println("\t" + out);
			}
			else
			{
				System.out.print(out);
			}

			i++;
		}
		System.out.println("***** End status *****\n\n");
	}
}
