package stibas14.pg4100.assignment1;

/**
 * This class implements RentalItem and is nothing more than a value-object that
 * can be stocked by a RentalService. It has a single identifier that should be
 * used to differentiate between similar objects.
 */
public class RentalCar implements RentalItem {

	private String identifier;

	public RentalCar(String identifier)
	{
		this.identifier = identifier;
	}

	@Override
	public String getIdentifier()
	{
		return identifier;
	}
}
