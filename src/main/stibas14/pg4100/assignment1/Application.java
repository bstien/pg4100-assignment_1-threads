package stibas14.pg4100.assignment1;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * PG4100
 * Innlevering 1
 * Threads
 *
 * Written by Bastian Stien
 */
public class Application {

	private final CountDownLatch latch;

	private final ArrayList<Customer> customers;

	private final ExecutorService executorService;

	private final CarRentalConsolePrinter consolePrinter;

	private final CarRentalService rentalService;

	public static void main(String[] args)
	{
		Application application = new Application();
		application.start();
	}

	public Application()
	{
		consolePrinter = new CarRentalConsolePrinter();
		rentalService = new CarRentalService();
		executorService = Executors.newCachedThreadPool();

		customers = new ArrayList<>();

		// Start the thread waiting for the first 5 customers.
		latch = new CountDownLatch(5);
		(new Thread(new ThreadStarter())).start();

		rentalService.addObserver(consolePrinter);

		// IntelliJ suggested this lambda. Original code below.
		// for (RentalCar car : initCars(5)) { rentalService.addItem(car); }
		initCars(5).forEach(rentalService::addItem);
	}

	public void start()
	{
		try (Scanner in = new Scanner(System.in))
		{
			for (int i = 0; i < 10; i++)
			{
				System.out.print("Enter new name: ");
				String name = in.next().trim();
				Customer customer = new Customer(name);
				customer.setRentalService(rentalService);

				customers.add(customer);
				latch.countDown();

				// If we have more than 5 customers we have to instantiate the
				// threads manually.
				if ( i >= 5 )
				{
					executorService.execute(customer);
				}
			}
		}
	}

	public ArrayList<RentalCar> initCars(int howMany)
	{
		ArrayList<RentalCar> cars = new ArrayList<>();

		for (int i = 0; i <= howMany; i++)
		{
			String identifier = "" + i + i + i + i;
			RentalCar car = new RentalCar(identifier);

			cars.add(car);
		}

		return cars;
	}

	private class ThreadStarter implements Runnable {

		@Override
		public void run()
		{
			try
			{
				latch.await();
				for (int i = 0; i < 5; i++)
				{
					executorService.execute(customers.get(i));
				}
			} catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}
}
