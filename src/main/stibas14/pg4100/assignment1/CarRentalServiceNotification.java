package stibas14.pg4100.assignment1;

/**
 * A class that holds information related to an event sent out by
 * CarRentalService. It exists to simplify the notification of subscribers.
 */
public class CarRentalServiceNotification {

	private String type;

	private Customer customer;

	private String carIdentifier;

	public CarRentalServiceNotification(String type, Customer customer, String carIdentifier)
	{
		this.type = type;
		this.customer = customer;
		this.carIdentifier = carIdentifier;
	}

	public String getType()
	{
		return type;
	}

	public Customer getCustomer()
	{
		return customer;
	}

	public String getCarIdentifier()
	{
		return carIdentifier;
	}
}
