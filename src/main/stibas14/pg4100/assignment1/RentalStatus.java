package stibas14.pg4100.assignment1;

/**
 * The purpose of this class is to easily keep track of an items status
 * regarding availability.
 */
public class RentalStatus {

	// Probably misspelled, but I think it's understandable.
	private String rentee = null;

	public void setUnavailable(String rentee) throws IllegalArgumentException
	{
		// Don't change identifier when already unavailable.
		if ( !isAvailable() )
		{
			return;
		}

		// Don't accept null or empty strings.
		if ( rentee == null || rentee.length() == 0 )
		{
			throw new IllegalArgumentException("Identifier for rentee must not be null or empty string.");
		}

		this.rentee = rentee;
	}

	/**
	 * Call this method to signalize that the item is available for others to
	 * use.
	 */
	public void setAvailable()
	{
		rentee = null;
	}

	/**
	 * Check if the item is available for others to use.
	 *
	 * @return True if available, false otherwise
	 */
	public boolean isAvailable()
	{
		return rentee == null;
	}

	/**
	 * Return the unique identifier for the rentee.
	 *
	 * @return The identifier for the rentee
	 */
	public String getRenteeIdentifier()
	{
		return rentee;
	}
}