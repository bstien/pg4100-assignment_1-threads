package stibas14.pg4100.assignment1;

import java.util.ArrayList;
import java.util.Map;

/**
 * A simple interface to be used by services that should be able to rent out
 * items to customers.
 */
public interface RentalService {

	/**
	 * Add an item to this RentalService.
	 *
	 * @param item
	 */
	void addItem(RentalItem item);

	/**
	 * Let a Customer rent an item.
	 *
	 * @param customer
	 */
	void rentItem(Customer customer);

	/**
	 * Let a Customer return a rented item.
	 *
	 * @param customer
	 */
	void returnItem(Customer customer);

	/**
	 * Return a list of all RentalItems available by this RentalService.
	 */
	ArrayList<RentalItem> inventory();

	/**
	 * Return a list of each RentalItems unique identifier along with its
	 * RentalStatus.
	 */
	Map<String, RentalStatus> rentalStatus();

	/**
	 * Check if a specific Customer has an active rental in this RentalService.
	 *
	 * @param customer
	 */
	boolean hasActiveRental(Customer customer);

}
