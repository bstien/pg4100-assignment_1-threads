package stibas14.pg4100.assignment1;

/**
 * A class that represents a Customer. It will try to rent and return items on a
 * regular basis, but doesn't care what type of items. Give it an instance of a
 * RentalService and it should prove itself to be a loyal customer :)
 */
public class Customer implements Runnable {

	private String name;

	private RentalService rentalService;

	private volatile boolean keepRunning;

	public Customer(String name)
	{
		this.name = name;
		this.keepRunning = true;
	}

	/**
	 * Return the name, or unique identifier, for this Customer.
	 *
	 * @return The unique identifier
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Set a RentalService to relate to this Customer.
	 *
	 * @param rentalService The RentalService
	 */
	public void setRentalService(RentalService rentalService)
	{
		this.rentalService = rentalService;
	}

	/**
	 * Get the RentalService related to this Customer.
	 *
	 * @return The related RentalService
	 */
	public RentalService getRentalService()
	{
		return rentalService;
	}

	/**
	 * Get a random number of ms to sleep between 1000ms and 1000ms+offset. For
	 * a number between 1.000 and 10.000, give 9.000 as offset.
	 * (1.000+9.000=10.000).
	 *
	 * @param offset Range of ms to sleep
	 * @return A number between 1000 and 1000 + offset
	 */
	public int getSleepTime(int offset)
	{
		return (int) (Math.random() * offset + 1000);
	}

	/**
	 * keepRunning is a simple boolean flag. Call this method to cancel the
	 * execution of the thread.
	 */
	public void cancel()
	{
		keepRunning = false;
	}

	@Override
	public void run()
	{
		while (keepRunning)
		{
			// Sleep some time between 1 and 10 seconds.
			int sleepTime = getSleepTime(9000);
			try
			{
				Thread.sleep(sleepTime);
			} catch (InterruptedException e)
			{
				e.printStackTrace();
			}

			rentalService.rentItem(this);

			// Sleep some time between 1 and 3 seconds.
			sleepTime = getSleepTime(2000);
			try
			{
				Thread.sleep(sleepTime);
			} catch (InterruptedException e)
			{
				e.printStackTrace();
			}

			rentalService.returnItem(this);
		}
	}
}
