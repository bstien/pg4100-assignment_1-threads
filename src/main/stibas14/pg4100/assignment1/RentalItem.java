package stibas14.pg4100.assignment1;

/**
 * A simple interface that items that can be rented should implement.
 */
public interface RentalItem {

	String getIdentifier();
}
