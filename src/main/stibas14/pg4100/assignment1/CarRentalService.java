package stibas14.pg4100.assignment1;

import java.util.*;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * An implementation of RentalService that rents out cars to customers. The
 * class also extends 'java.util.Observable' so it can notify eventual
 * subscribers when an event occurs.
 */
public class CarRentalService extends Observable implements RentalService {

	private Map<String, RentalStatus> rentalStatus;

	private ArrayList<RentalItem> inventory;

	private Lock rentalLock = new ReentrantLock();

	private Condition availableCarsCondition = rentalLock.newCondition();

	public CarRentalService()
	{
		inventory = new ArrayList<>();
		rentalStatus = Collections.synchronizedMap(new HashMap<String, RentalStatus>());
	}

	@Override
	public void addItem(RentalItem newCar)
	{
		// Only accept instances of RentalCar.
		if ( !(newCar instanceof RentalCar) )
		{
			return;
		}

		// Only accept unique identifiers in inventory.
		for (RentalItem car : inventory)
		{
			if ( newCar.getIdentifier().equals(car.getIdentifier()) )
			{
				return;
			}
		}

		inventory.add(newCar);

		RentalStatus available = new RentalStatus();
		rentalStatus.put(newCar.getIdentifier(), available);
	}

	@Override
	public void rentItem(Customer customer)
	{
		rentalLock.lock();
		try
		{
			// Deny a customer to rent several items.
			if ( hasActiveRental(customer) )
			{
				return;
			}

			// Stop execution of thread when no cars are available.
			while (!hasCarAvailable())
			{
				notifyNoAvailableCar(customer);
				availableCarsCondition.await();
			}

			String carIdentifier = firstAvailableCar();

			RentalStatus status = rentalStatus.get(carIdentifier);
			status.setUnavailable(customer.getName());
			rentalStatus.put(carIdentifier, status);

			// Notify subscribers.
			notifyRental(customer, carIdentifier);
		} catch (InterruptedException e)
		{
			e.printStackTrace();
		} finally
		{
			rentalLock.unlock();
		}
	}

	@Override
	public void returnItem(Customer customer)
	{
		rentalLock.lock();
		try
		{
			// A customer can't return any items when it hasn't any active rentals.
			if ( !hasActiveRental(customer) )
			{
				return;
			}

			String carIdentifier = getCustomersRentalCar(customer);
			RentalStatus status = rentalStatus.get(carIdentifier);
			status.setAvailable();
			rentalStatus.put(carIdentifier, status);

			// Signal the condition in case any threads are waiting.
			availableCarsCondition.signal();

			// Notify subscribers.
			notifyReturn(customer, carIdentifier);
		} finally
		{
			rentalLock.unlock();
		}

	}

	/**
	 * Check if there's any cars available to rent.
	 *
	 * @return true if cars available, false otherwise
	 */
	private boolean hasCarAvailable()
	{
		return firstAvailableCar() != null;
	}

	private String firstAvailableCar()
	{
		for (String key : rentalStatus.keySet())
		{
			if ( rentalStatus.get(key).isAvailable() )
			{
				return key;
			}
		}

		return null;
	}

	/**
	 * Get the car rented by a specific Customer, if any. Null is returned if
	 * the Customer has no active rentals.
	 *
	 * @param customer
	 * @return String or null
	 */
	private String getCustomersRentalCar(Customer customer)
	{
		for (String identifier : rentalStatus.keySet())
		{
			if ( customer.getName().equals(rentalStatus.get(identifier).getRenteeIdentifier()) )
			{
				return identifier;
			}
		}

		return null;
	}

	@Override
	public Map<String, RentalStatus> rentalStatus()
	{
		return rentalStatus;
	}

	@Override
	public boolean hasActiveRental(Customer customer)
	{
		return getCustomersRentalCar(customer) != null;
	}

	@Override
	public ArrayList<RentalItem> inventory()
	{
		return inventory;
	}

	public boolean isCarAvailable(String carIdentifier)
	{
		return rentalStatus.get(carIdentifier).isAvailable();
	}


	/**
	 * Notify subscribers that a rental has successfully been completed.
	 *
	 * @param customer
	 * @param carIdentifier
	 */
	public void notifyRental(Customer customer, String carIdentifier)
	{
		setChanged();
		CarRentalServiceNotification notification =
				new CarRentalServiceNotification("rental", customer, carIdentifier);
		notifyObservers(notification);
	}

	/**
	 * Notify subscribers that a return has successfully been completed.
	 *
	 * @param customer
	 * @param carIdentifier
	 */
	public void notifyReturn(Customer customer, String carIdentifier)
	{
		setChanged();
		CarRentalServiceNotification notification =
				new CarRentalServiceNotification("return", customer, carIdentifier);
		notifyObservers(notification);
	}

	/**
	 * Notify subscribers that a Customer has tried to rent a car, but none was
	 * available.
	 *
	 * @param customer
	 */
	public void notifyNoAvailableCar(Customer customer)
	{
		setChanged();
		CarRentalServiceNotification notification =
				new CarRentalServiceNotification("no_available_cars", customer, null);
		notifyObservers(notification);
	}
}
