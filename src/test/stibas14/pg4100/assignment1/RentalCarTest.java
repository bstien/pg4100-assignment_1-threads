package stibas14.pg4100.assignment1;

import org.junit.Test;

import static org.junit.Assert.*;

public class RentalCarTest {

	@Test
	public void it_has_an_identifier() throws Exception
	{
		String identifier = "YE 61788";
		RentalCar car = new RentalCar(identifier);

		assertEquals(identifier, car.getIdentifier());
	}
}