package stibas14.pg4100.assignment1;

import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class CustomerTest {

	@Test
	public void it_should_have_a_name() throws Exception
	{
		Customer customer = new Customer("Bastian");

		assertTrue("Bastian".equals(customer.getName()));
	}

	@Test
	public void it_should_have_a_car_rental_service() throws Exception
	{
		Customer customer = new Customer("Bastian");
		RentalService rentalService = mock(RentalService.class);

		customer.setRentalService(rentalService);

		assertNotNull(customer.getRentalService());
	}

	@Test
	public void it_should_be_able_to_rent_a_car_and_return_it() throws Exception
	{
		CarRentalService rentalService = spy(new CarRentalService());
		final Customer customer = spy(new Customer("foo"));

		customer.setRentalService(rentalService);
		rentalService.addItem(new RentalCar("111"));

		// Return 0 so the thread won't sleep.
		doReturn(0).when(customer).getSleepTime(anyInt());

		// Cancel the Customer-thread on returning item.
		// IntelliJ suggested lambda, so I went with it. No need for anonymous classes! :D
		doAnswer(invocation -> {
			customer.cancel();
			return null;
		}).when(rentalService).returnItem(any(Customer.class));

		// Create the thread, start it and wait for it to finish.
		Thread t1 = new Thread(customer);
		t1.start();
		t1.join();

		verify(rentalService).rentItem(any(Customer.class));
		verify(rentalService).returnItem(any(Customer.class));
	}

	@Test
	public void it_should_return_correct_sleep_time() throws Exception
	{
		// This one is hard to test, so I'll just do some quick
		// asserts that we have a ballpark result.
		Customer customer = new Customer("foo");

		int between_one_and_three = customer.getSleepTime(2);
		assertTrue(between_one_and_three >= 1000 && between_one_and_three <= 3000);

		int between_one_and_ten = customer.getSleepTime(9);
		assertTrue(between_one_and_ten >= 1000 && between_one_and_ten <= 10000);

		int exactly_one_second = customer.getSleepTime(0);
		assertEquals(1000, exactly_one_second);
	}
}