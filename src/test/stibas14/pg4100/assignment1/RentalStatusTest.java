package stibas14.pg4100.assignment1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RentalStatusTest {

	private RentalStatus rentalStatus;

	@Before
	public void setUp() throws Exception
	{
		rentalStatus = new RentalStatus();
	}

	@Test ( expected = IllegalArgumentException.class )
	public void it_should_not_accept_a_null_identifier() throws Exception
	{
		rentalStatus.setUnavailable(null);
	}

	@Test ( expected = IllegalArgumentException.class )
	public void it_should_not_accept_an_empty_identifier() throws Exception
	{
		rentalStatus.setUnavailable("");
	}

	@Test
	public void it_should_return_correct_rentee() throws Exception
	{
		String identifier = "Bastian";
		rentalStatus.setUnavailable(identifier);

		assertEquals(identifier, rentalStatus.getRenteeIdentifier());
	}

	@Test
	public void it_should_be_available_when_instantiated() throws Exception
	{
		assertTrue(rentalStatus.isAvailable());
	}

	@Test
	public void it_should_be_unavailable_when_set_unavailable() throws Exception
	{
		rentalStatus.setUnavailable("foo");

		assertFalse(rentalStatus.isAvailable());
	}

	@Test
	public void it_should_be_available_when_its_status_changes() throws Exception
	{
		rentalStatus.setUnavailable("foo");
		rentalStatus.setAvailable();

		assertTrue(rentalStatus.isAvailable());
	}


	@Test
	public void it_should_not_change_its_identifier_when_already_unavailable() throws Exception
	{
		rentalStatus.setUnavailable("foo");
		rentalStatus.setUnavailable("bar");

		assertEquals("foo", rentalStatus.getRenteeIdentifier());
	}
}