package stibas14.pg4100.assignment1;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class CarRentalServiceTest {

	private CarRentalService rentalService;

	@Before
	public void setUp() throws Exception
	{
		rentalService = new CarRentalService();
	}

	@Test
	public void it_should_let_items_be_added_to_inventory() throws Exception
	{
		RentalCar car = new RentalCar("AAA");
		rentalService.addItem(car);

		assertEquals(1, rentalService.inventory().size());
	}

	@Test
	public void it_should_not_let_two_identical_items_be_added_to_inventory() throws Exception
	{
		RentalCar car1 = new RentalCar("AAA");
		RentalCar car2 = new RentalCar("AAA");

		rentalService.addItem(car1);
		rentalService.addItem(car2);

		assertEquals(1, rentalService.inventory().size());
	}

	@Test
	public void it_should_only_accept_rental_cars() throws Exception
	{
		RentalItem item = mock(RentalItem.class);
		rentalService.addItem(item);

		assertEquals(0, rentalService.inventory().size());
	}

	@Test
	public void it_should_return_rental_status_for_its_inventory() throws Exception
	{
		assertNotNull(rentalService.rentalStatus());
	}

	@Test
	public void its_rental_status_should_reflect_actual_status() throws Exception
	{
		Customer customer = new Customer("foo");

		RentalCar car1 = new RentalCar("AAA");

		rentalService.addItem(car1);
		rentalService.rentItem(customer);

		assertFalse("A supposedly rented car is still available", rentalService.isCarAvailable("AAA"));

		rentalService.returnItem(customer);

		assertTrue("A supposedly returned car is still unavailable", rentalService.isCarAvailable("AAA"));
	}

	@Test
	public void it_should_not_let_a_customer_rent_several_cars() throws Exception
	{
		Customer customer = new Customer("foo");
		RentalCar car1 = new RentalCar("AAA");
		RentalCar car2 = new RentalCar("BBB");

		rentalService.addItem(car1);
		rentalService.rentItem(customer);

		rentalService.addItem(car2);
		rentalService.rentItem(customer);

		assertTrue(rentalService.isCarAvailable("BBB"));
	}

	@Test
	public void it_should_notify_its_subscribers_when_a_car_is_rented() throws Exception
	{
		Customer customer = new Customer("foo");
		RentalCar car = new RentalCar("AAA");

		rentalService = spy(rentalService);

		rentalService.addItem(car);
		rentalService.rentItem(customer);

		verify(rentalService).notifyRental(any(Customer.class), anyString());
	}

	@Test
	public void it_should_notify_its_subscribers_when_a_car_is_returned() throws Exception
	{
		Customer customer = new Customer("foo");
		RentalCar car = new RentalCar("AAA");

		rentalService = spy(rentalService);

		rentalService.addItem(car);
		rentalService.rentItem(customer);
		rentalService.returnItem(customer);

		verify(rentalService).notifyReturn(any(Customer.class), anyString());
	}
}