## Innlevering 1: Threads
_Levert av Bastian Stien_

### Hva har jeg lært?
I denne innleveringen har jeg virkelig fått gjort mye, og det setter jeg stor pris på. I motsetning til fjoråret hvor hver innlevering var ekvivalenten til 'prikk-til-prikk' var dette mer givende, både med tanke på selve oppgaven, men også til hvilke kunnskaper som kreves for å ta en vurdering på valgene man har foran seg.

De siste ukene har mye tid gått med på å lese artikler og se filmer for å få bedre forståelse på hva som faktisk skjer og hvordan man bør håndtere tråder.

Jeg har fått forståelse for at tråder er uforutsigbare, men at vi ved hjelp av `Condition`s og synkroniserte attributter kan gjøre multithreading til en relativt smertefri affære.

### Hva kunne vært gjort bedre?
Det er mye jeg kunne gjort annerledes, men jeg tror valgene jeg til nå har gjort er de riktige. Men spør meg igjen om 6 måneder så tror jeg du får et helt annet svar. Øvelse gjør mester.


### Forutsetninger for å kjøre
Prosjektet mitt er basert på Gradle, og da dette er såpass utbredt antar jeg at det fins kunnskap nok til å kunne bygge et slikt prosjekt, men jeg tar med kommandoene uansett.

Da jeg har tatt i bruk et par nye ting fra Java 8 har jeg også ført opp dette i `build.gradle`. Sørg for at dette er installert for deg som skal teste.

Gradle-wrapper er inkludert, så du trenger ikke ha dette installert for å kjøre. 

```bash
# For å bygge prosjektet, samt hente ned dependencies
./gradlew build

# For å produsere prosjektfiler for IntelliJ, skulle det være aktuelt
./gradlew idea
```
